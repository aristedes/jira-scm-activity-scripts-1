#!/usr/bin/env python

__author__ = "scmenthusiast@gmail.com"
__version__ = "1.0"

import argparse
import MySQLdb
import json
import sys
import os


class ScmActivityImport(object):
    def __init__(self):
        """
        __init__(). prepares or initiates configuration file
        :param: None
        :return: None
        """

        our_config_file = os.path.join(os.path.dirname(__file__), "config.json")

        if os.path.exists(our_config_file):

            try:
                config = json.load(open(our_config_file))

                self.target_db_host = config.get("target.database.host")
                self.target_db_username = config.get("target.database.username")
                self.target_db_password = config.get("target.database.password")
                self.target_db_name = config.get("target.database.name")

                self.scm_activity_tbl = "scm_activity"
                self.scm_message_tbl = "scm_message"
                self.scm_files_tbl = "scm_files"
                self.scm_job_tbl = "scm_job"

            except ValueError, e:
                print (e)
                sys.exit(1)
        else:
            print("Error: config.json file not exists in current path!")
            sys.exit(1)

    def run(self, import_file, debug=False):
        """
        run(). to change sets from import file
        :param import_file: import file
        :param debug: For real run
        :return: None
        """

        if os.path.exists(import_file):
            change_set_list = json.load(open(import_file))
        else:
            print "Error: Input File {0} is not exists!".format(import_file)
            sys.exit(1)

        connection = MySQLdb.connect(self.target_db_host, self.target_db_username, self.target_db_password, self.target_db_name)

        cursor = connection.cursor(MySQLdb.cursors.DictCursor)

        sql_1 = "INSERT INTO {0} (issueKey, changeId, changeType, changeDate, changeAuthor, changeLink, " \
              "changeBranch, changeTag, changeStatus) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %s )".format(self.scm_activity_tbl);

        sql_2 = "INSERT INTO {0} (message, scmActivityID) VALUES (%s, %s)".format(self.scm_message_tbl)

        sql_3 = "INSERT INTO {0} (fileName, fileAction, fileVersion, scmActivityID) VALUES (%s, %s, %s, %s)".format(self.scm_files_tbl)

        sql_4 = "INSERT INTO {0} (jobName, jobStatus, jobLink, scmActivityID) VALUES (%s, %s, %s, %s)".format(self.scm_job_tbl)

        for change in change_set_list:

            issue_key = change.get("issueKey")
            change_id = change.get("changeId")
            change_type = change.get("changeType")
            change_date = change.get("changeDate")
            change_author = change.get("changeAuthor")
            change_link = change.get("changeLink")
            change_branch = change.get("changeBranch")
            change_tag = change.get("changeTag")
            change_status = change.get("changeStatus")
            change_message = change.get("changeMessage")
            change_files = change.get("changeFiles")
            scm_jobs = change.get("scmJobs")

            print "[Info] Processing SCM Activity > {0} > {1} > {2}".format(issue_key, change_id, change_type)

            if not issue_key and not change_id and not change_type and \
                    not change_date and not change_author:
                print "{0:>3} [Error] Required inputs are missing. Skipping!".format('')
                continue

            cursor.execute("SELECT * FROM {0} WHERE issueKey=%s AND changeId=%s AND changeType=%s".format(self.scm_activity_tbl),
                           [issue_key, change_id, change_type])

            exist_row = cursor.fetchone()

            if exist_row:
                exist_row_id = exist_row.get("ID")
                print "{0:>3} [Warn] Row ID [{1}] is exists. Skipping!".format('', exist_row_id)
                continue

            result = 0

            if debug is True:
                result = cursor.execute(sql_1, [issue_key, change_id, change_type, change_date, change_author, change_link,
                                            change_branch, change_tag, change_status])

            if result == 1:

                scm_id = cursor.lastrowid

                print "{0:>3} > [{1}] Created SCM Activity".format('', scm_id)

                if change_message and change_message != "":
                    print "{0:>3} > [{1}] Updating Change Message".format('', scm_id)
                    cursor.execute(sql_2, [change_message, scm_id])

                if change_files and len(change_files) > 0:
                    print "{0:>3} > [{1}] Updating Change Files ({2})".format('', scm_id, len(change_files))
                    for scm_file in change_files:
                        file_name = scm_file.get("fileName")
                        file_action = scm_file.get("fileAction")
                        file_version = scm_file.get("fileVersion")
                        cursor.execute(sql_3, [file_name, file_action, file_version, scm_id])

                if scm_jobs and len(scm_jobs) > 0:
                    print "{0:>3} > [{1}] Updating SCM Jobs ({2})".format('', scm_id, len(scm_jobs))
                    for job in scm_jobs:
                        job_name = job.get("jobName")
                        job_status = job.get("jobStatus")
                        job_link = job.get("jobLink")
                        cursor.execute(sql_4, [job_name, job_status, job_link, scm_id])

                connection.commit()

        print "[Info] Total Change Sets > {0}".format(len(change_set_list))


def main():
    parser = argparse.ArgumentParser(description='SCM Activity Database Import Script')
    parser.add_argument("--import-file", help='Required Import File Path`', required=True)
    parser.add_argument("--debug", help='For real run', action='store_true')
    args = parser.parse_args()

    j = ScmActivityImport()
    j.run(args.import_file, args.debug)

if __name__ == '__main__':
    main()
