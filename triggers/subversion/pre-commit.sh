#!/usr/bin/env bash

REPOS="$1"
TXN="$2"

# define required vars
python_path=python
svn_hook=`dirname $0`/jirahooks/svn_pre_commit.py

# execute
${python_path} ${svn_hook} --config "svn_cfg.json" --repos ${REPOS} --txn ${TXN} || exit 1

exit 0