#!/usr/bin/env python

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from jira_commit_lib import JiraCommitHook
from json import load, loads, dumps
import argparse
import sys
import os


class GitCommitMsgHook(object):

    def __init__(self, config):
        """
        __init__(). prepares or initiates configuration file
        :return: None
        """

        if not config: config = "git_cfg.json" # default config file

        self.config_file = os.path.join(os.path.dirname(__file__), config)

        if self.config_file and os.path.exists(self.config_file):
            try:
                configuration = load(open(self.config_file))

                self.git_cmd = configuration.get("git.cmd.path")
                if not os.path.exists(self.git_cmd):
                    self.git_cmd = "git"

                self.jch = JiraCommitHook(configuration)

            except ValueError, e:
                print(e)
                sys.exit(1)
        else:
            print "[Error] Config file ({0}) not exists!".format(config)
            sys.exit(1)

    def run(self, message_file):
        """
        run(). executes the jira update for the given change-set
        :param message_file: Git message file
        :return: None
        """

        with open(message_file) as f_content:
            git_message = f_content.read()

        if git_message:

            matched_issue_keys = self.jch.pattern_validate(git_message)

            if len(matched_issue_keys.keys()) > 0:
                index = self.jch.jira_pre_validate(matched_issue_keys.keys())
                if index == 0:
                    print("[POLICY] *** Required at-least one Valid JIRA Issue ID.")
                    sys.exit(1)
            else:
                print "[POLICY] *** Required at-least one JIRA Issue ID. To skip use [JIRA NONE]"
                sys.exit(1)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """

    parser = argparse.ArgumentParser(description='Git Commit Msg Hook')
    parser.add_argument("--config", help='Hook Config file')
    parser.add_argument("--message-file", help='Git Message File', required=True)

    args = parser.parse_args()

    g = GitCommitMsgHook(args.config)
    g.run(args.message_file)


if __name__ == '__main__':
    main()