#!/usr/bin/env python

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from jira_commit_lib import JiraCommitHook
from jira_commit_lib import ScmActivity
from json import load, loads, dumps
from datetime import datetime
import argparse
import sys
import os


class GitPreReceiveHook(object):

    def __init__(self, config):
        """
        __init__(). prepares or initiates configuration file
        :return: None
        """

        if not config: config = "git_cfg.json" # default config file

        self.config_file = os.path.join(os.path.dirname(__file__), config)

        if self.config_file and os.path.exists(self.config_file):
            try:
                configuration = load(open(self.config_file))

                self.git_cmd = configuration.get("git.cmd.path")
                if not os.path.exists(self.git_cmd):
                    self.git_cmd = "git"

                self.jch = JiraCommitHook(configuration)

            except ValueError, e:
                print(e)
                sys.exit(1)
        else:
            print "[Error] Config file ({0}) not exists!".format(config)
            sys.exit(1)

    def run(self, oldrev, newrev, refname):
        """
        run(). executes the jira update for the given change-set
        :param oldrev: Git old revision
        :param newrev: Git new revision
        :param refname: Git branch name
        :return: None
        """

        git_revision_list = self.jch.command_output("{0} rev-list {1}...{2}".format(self.git_cmd, oldrev, newrev))

        for revision in git_revision_list.split("\n"):

            git_message = self.jch.command_output("{0} show -s --pretty=format:\"%B\" {1}".format(self.git_cmd, revision))

            if git_message:
                matched_issue_keys = self.jch.pattern_validate(git_message)

                'print matched_issue_keys'

                if len(matched_issue_keys) != 0:
                    index = self.jch.jira_pre_validate(matched_issue_keys.keys())
                    if index == 0:
                        print("[POLICY] *** {0} - Required at-least one Valid JIRA Issue ID.".format(revision))
                        sys.exit(1)
                else:
                    print("[POLICY] *** Required at-least one JIRA Issue ID. To skip use [JIRA NONE]")
                    sys.exit(1)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """

    parser = argparse.ArgumentParser(description='SCM Activity GIT Pre Receive Hook Execution Script')
    parser.add_argument("--config", help='Optional config file')
    parser.add_argument("--oldrev", help='Required oldrev', required=True)
    parser.add_argument("--newrev", help='Required newrev', required=True)
    parser.add_argument("--refname", help='Required refname', required=True)

    args = parser.parse_args()

    # oldrev, newrev, refname = sys.stdin.read().split()

    if args.oldrev and args.newrev and args.refname:
        g = GitPreReceiveHook(args.config)
        g.run(args.oldrev, args.newrev, args.refname)
    else:
        print "[usage] post-receive oldrev newrev refname"
        sys.exit(1)


if __name__ == '__main__':
    main()
