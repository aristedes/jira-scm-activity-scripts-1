#!/usr/bin/env python

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from jira_commit_lib import JiraCommitHook
from jira_commit_lib import ScmActivity
from json import load, loads, dumps
from datetime import datetime
import sys
import re
import os


"""
    This python hook will trigger when new change is added on Gerrit.
    Parameters:
        - Commit SHA1 (Required)
        - Configuration file name (Optional - default is coco_cfg.json)
"""


class GerritGenericHook(object):

    def __init__(self, config='gerrit_cfg.json'):
        """
        __init__(). prepares or initiates configuration file
        :param config: config file
        :return: None
        """

        self.config_file = os.path.join(os.path.dirname(__file__), config)

        if os.path.exists( self.config_file ):
            try:
                config = load(open(self.config_file))

                self.gerrit_server = config.get("gerrit.host")
                self.gerrit_name = config.get("gerrit.name")
                self.gerrit_port = config.get("gerrit.port")
                self.ssh_cmd = config.get("gerrit.ssh.path")
                if not os.path.exists(self.ssh_cmd):
                    self.ssh_cmd = "ssh"

                self.jch = JiraCommitHook(config)

            except ValueError, e:
                print (e)
                exit(1)
        else:
            print("Error: Config file not exists!")
            exit(1)

    def get_gerrit_change_set(self, change):
        """
        get_gerrit_change(). get scm change activity for given commit sha id
        :param change: Gerrit Change Item
        :return: object
        """

        changeset = ScmActivity()
        changeset.changeId = change.get("number")

        # Preferred format: ChangeType_Instance Name e.g. gerrit_ccx
        if self.gerrit_name:
            changeset.changeType = "{0}_{1}".format("gerrit", self.gerrit_name)
        else:
            changeset.changeType = "gerrit"

        changeset.changeAuthor = change.get("owner").get("username")
        changeset.changeDate = datetime.fromtimestamp(change.get("createdOn")).strftime('%Y-%m-%d %H:%M:%S')
        changeset.changeLink = change.get("url")
        changeset.changeStatus = change.get("status")
        changeset.changeBranch = change.get("branch")
        changeset.changeTag = change.get("topic")

        # set message
        change_message = []
        change_message.append("{0}".format(change.get("commitMessage").strip()))
        change_message.append("Created On: {0}".format(datetime.fromtimestamp(int(change.get("createdOn"))).strftime('%Y-%m-%d %H:%M:%S')))
        change_message.append("Project: {0}".format(change.get("project")))

        patch_sets = change.get("patchSets")
        patch_set_len = len(change.get("patchSets"))

        if patch_set_len > 0:
            patch_set = patch_sets[-1]
            if patch_set.get("approvals"):
                patch_approvals = patch_set.get("approvals")
                if len(patch_approvals) > 0:
                    change_message.append("||Approver||Type||Score||")
                    for review in patch_approvals:
                        if review.get("type") == 'SUBM': continue

                        review_value = review.get("value")
                        if re.search("-", review_value):
                            review_value = "{{color:red}}{0}{{color}}".format(review_value)
                        else:
                            review_value = "{{color:green}}+{0}{{color}}".format(review_value)

                        change_message.append("| {0} | {1} | {2} |".format(review.get("by").get("name"),
                                review.get("type"), review_value))

        comments = change.get("comments")
        if comments:
            if len(comments) > 0:
                last_comment = comments[-1]
                last_comment_text = os.linesep.join([s for s in last_comment.get("message").splitlines() if s])
                timestamp = datetime.fromtimestamp(int(last_comment.get("timestamp"))).strftime('%Y-%m-%d %H:%M:%S')
                change_message.append("(i) Last update by {0} on {2}\n{1}".format(last_comment.get("reviewer").get("name"),
                            last_comment_text, timestamp))

        changeset.changeMessage = '\n'.join(change_message)

        # set affected files
        '''patch_sets = change.get("patchSets")
        patch_set_len = len(change.get("patchSets"))
        change_affected = []

        if patch_set_len > 0:
            patch_set = patch_sets[-1]
            if patch_set.get("files"):
                file_list = patch_set.get("files")
                if len(file_list) > 0:
                    for file_item in file_list:
                        if file_item.get("file") == '/COMMIT_MSG': continue
                        # change_affected.append("{0:>8}  {1}".format(file.get("type"), file.get("file")))
                        file_object = {
                            "fileName" : file_item.get("file"),
                            "fileAction" : file_item.get("type")
                        }
                        change_affected.append(file_object)

        changeset.changeFiles =  change_affected'''

        return changeset

    def run(self, commit_sha1):
        """
        run(). executes the jira update for the given change-set
        :param commit_sha1: change commit sha1 id
        :return: None
        """

        gerrit_change = None
        gerrit_change_out = self.jch.command_output("{0} -q -p {1} {2} gerrit query --format=JSON --comments --current-patch-set --files --all-approvals commit:{3}"
                    .format(self.ssh_cmd, self.gerrit_port, self.gerrit_server, commit_sha1))

        output_content = gerrit_change_out.strip().split("\n")
        if len(output_content) == 2:
            gerrit_change = output_content[0]

        if gerrit_change:
            change = loads(gerrit_change)

            print dumps(change, indent=4)

            gerrit_change_set = self.get_gerrit_change_set(change)
            matched_issue_keys = self.jch.pattern_validate(change.get("subject"))

            print dumps(gerrit_change_set.__dict__, indent=4)
            print matched_issue_keys

            if len(matched_issue_keys.keys()) > 0:
                self.jch.jira_update(gerrit_change_set, matched_issue_keys.keys(),1)

        else:
            print "Warn: unable to get commit details for - {0}".format(commit_sha1)


def get_opts(argv):
    """
    get_opts(). converts sys arguments to dictionary
    :param argv: sys args
    :return: dict
    """

    opts = {}

    while argv:
        if argv[0][0] == '-':
            opts[argv[0]] = argv[1]
            argv = argv[2:]
        else:
            argv = argv[1:]
    return opts


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """

    args = get_opts(sys.argv)

    if args.get("--commit") and args.get("--commit") != "":
        # g = GerritGenericHook("bits.json") # for custom configuration file
        g = GerritGenericHook() # this uses default config
        g.run(args.get("--commit"))
    elif args.get("--newrev") and args.get("--newrev") != "":
        # g = GerritGenericHook("bits.json") # for custom configuration file
        g = GerritGenericHook() # this uses default config
        g.run(args.get("--newrev"))
    else:
        print "Warn: --commit or --newrev args or its value is missing!"


if __name__ == '__main__':
    main()
